#include "Timer.h"
#include "prj.h"
#include "printf.h"

// 1. Specify the interfaces that we will use
module prjC @safe(){
	uses {
		interface Leds;
		interface Boot;
		interface Receive;
		interface AMSend;
		interface Timer<TMilli> as LoopTimer;
		interface SplitControl as AMControl;
		interface Packet;
	}
}

implementation{
	
	// 2. Define (support) variables
	message_t msg_buff;
	bool busy;
	uint16_t close_mote;
	
	// 3. Initialize & start the radio of the mote
	event void Boot.booted() {
		call AMControl.start();
	}

	event void AMControl.startDone(error_t err) {
		if (err == SUCCESS) {
			call LoopTimer.startPeriodic(500);
		}
		else {
			call AMControl.start(); // try again to start it
		} 
	}

	event void AMControl.stopDone(error_t err) { }
	  
	// 4. Application Logic 
	event void LoopTimer.fired() {
		
		if (busy) {
			return; // busy indicates that there is an ongoing sending
		} 
		else {
			
			IdMsg* payload = (IdMsg*)call Packet.getPayload(&msg_buff, sizeof(IdMsg));
			
			if (payload == NULL) {
				return; // emtpy msg
			} 
			else {
				// fill the message with the sender mote ID
				payload->id = TOS_NODE_ID;
				// start sending
				if (call AMSend.send(AM_BROADCAST_ADDR, &msg_buff, sizeof(IdMsg)) == SUCCESS) {
					// set the busy flag since there is an ongoing sending
					busy = TRUE;
				}
			}
		}
	}
	
	
	event void AMSend.sendDone(message_t* msg, error_t error) {
		if (&msg_buff == msg) { 
			// reset the busy flag since the sending is complete
			busy = FALSE; 

			// turn off the red LED when sending
			call Leds.led0Off();
		}
	}	
	
	// upon receival, message is decoded, the ID is stored in memory and an alarm is issued
	event message_t* Receive.receive(message_t* msg, void* payload, uint8_t len) {
		if (len != sizeof(IdMsg)) {
			return msg;
		}
		else {
			IdMsg* rcm = (IdMsg*)payload;

			// storing the received id into close mote memory
			close_mote = rcm->id;

			// switch on red light when motes are in proximity
			call Leds.led0On();

			// sending alarm through printf
			printf("Mote#%u: ALARM!! Mote#%u is in proximity!!\n", TOS_NODE_ID, close_mote);
			printfflush();
			
			return msg;
		}
	}
}
