#ifndef PRJ_H
#define PRJ_H

// message with single element (mote ID)
typedef nx_struct IdMsg {
  nx_uint16_t id
} IdMsg;

enum {
  AM_ID_MSG = 6
};

#endif
