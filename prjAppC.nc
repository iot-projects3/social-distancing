#include "prj.h"
#include "printf.h"

configuration prjAppC{}

implementation{
  components MainC, prjC as App, LedsC;
  components new AMSenderC(AM_ID_MSG);
  components new AMReceiverC(AM_ID_MSG);
  components new TimerMilliC();
  components PrintfC;
  components SerialStartC;
  components ActiveMessageC;
  
  // wiring of app components
  App.Boot -> MainC.Boot;
  App.Leds -> LedsC;
  App.Receive -> AMReceiverC;
  App.AMSend -> AMSenderC;
  App.AMControl -> ActiveMessageC;
  App.LoopTimer -> TimerMilliC;
  App.Packet -> AMSenderC;
}
